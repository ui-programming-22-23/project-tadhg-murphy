const canvas = document.getElementById("canvas")
const context = canvas.getContext("2d");

const scale = 2;
const width = 16;
const height = 18;
const scaledWidth = scale * width;
const scaledHeight = scale * height;
const walkLoop = [0, 1, 0, 2];
const frameLimit = 7;
const username = localStorage.getItem('username');


let currentLoopIndex = 0;
let frameCount = 0;
let currentDirection = 0;
let speed = 2;
let randomX = Math.abs(Math.floor(Math.random() * 1099) - 50);
let randomY = Math.abs(Math.floor(Math.random() * 499) - 50);

function addName() {
    let header = document.getElementById("main-header");
    header.innerHTML = "Hello " + username;
}

addName();

let character = new Image();
character.src = "assets/img/player-spritesheet.png";

function GameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.mvmtDirection = "None";
}

// Default Player
let player = new GameObject(character, 0, 0, 200, 200);
let foods = new GameObject(foodSprite, randomX, randomY, 100, 100);

// The GamerInput is an Object that holds the Current
// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input


function input(event) {

    if (event.type === "keydown") {
        switch (event.keyCode) {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break; //Left key
            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break; //Up key
            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break; //Right key
            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break; //Down key
            case 83:
                speed = 4;
                break;
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None");
        speed = 2;
    }
}
function update() {
    // console.log("Update");
    // Check Input
    if (gamerInput.action === "Up") {
        if (player.y < 0){
            console.log("player at top edge");
        }
        else{
            player.y -= speed; // Move Player Up
        }
        currentDirection = 1;
    } else if (gamerInput.action === "Down") {
        if (player.y + scaledHeight > canvas.height){
            console.log("player at bottom edge");
        }
        else{
            player.y += speed; // Move Player Down
        }
        currentDirection = 0;
    } else if (gamerInput.action === "Left") {
        if (player.x < 0){
            console.log("player at left edge");
        }
        else{
            player.x -= speed; // Move Player Left
        }
        currentDirection = 2;
    } else if (gamerInput.action === "Right") {
        if (player.x + scaledWidth > canvas.width){

        }
        else{
            player.x += speed; // Move Player Right
        }
        currentDirection = 3;
    } else if (gamerInput.action === "None") {
    }
}

function drawFrame(image, frameX, frameY, canvasX, canvasY) {
    context.drawImage(image,
                  frameX * width, frameY * height, width, height,
                  canvasX, canvasY, scaledWidth, scaledHeight);
}

function animate() {
    if (gamerInput.action != "None"){
        frameCount++;
        if (frameCount >= frameLimit) {
            frameCount = 0;
            currentLoopIndex++;
            if (currentLoopIndex >= walkLoop.length) {
                currentLoopIndex = 0;
            }
        }      
    }
    else{
        currentLoopIndex = 0;
    }
    drawFrame(player.spritesheet, walkLoop[currentLoopIndex], currentDirection, player.x, player.y);
}
function draw() {
    context.clearRect(0,0, canvas.width, canvas.height);
    animate();
}

function gameloop() {
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}
// Handle Active Browser Tag Animation
window.requestAnimationFrame(gameloop);

// https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame

window.addEventListener('keydown', input);
// disable the second event listener if you want continuous movement
window.addEventListener('keyup', input);